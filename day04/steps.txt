#stateful state (custom resources)

kubectl create ns mysql-operator

helm repo add presslabs https://presslabs.github.io/charts

helm template mysql-operator presslabs/mysql-operator > mysql-operator.yaml

helm install mysql-operator presslabs/mysql-operator -n mysql-operator

kubectl get crd

watch kubectl get all,pvc,sts -n mysql-operator

kubectl describe sts/mysql-operator -n mysql-operator

kubectl api-resources | grep mysql

kubectl get sc

kubectl apply -f bggdb-cluster.yaml -n bggns

watch kubectl get po,svc,sts,pvc -n bggns

kubectl exec po/bggdb-cluster-mysql-0 -n bggns -ti -- bash

kubectl port-forward svc/nwapp-svc 8080:8080 -n nwns

kubectl scale deploy/nwapp-deploy --replicas 2 -n nwns